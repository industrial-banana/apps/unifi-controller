# Unifi Controller
Deployment manifests for Unifi Controller in my Kubernetes homelab.

## CRD Dependencies
This deployment depends on the following CRDs being present on the API server.

- Longhorn persistent storage.
- Cert-Manager.
- Traefik IngressRoute
- Prometheus Operator